const amqp = require('amqplib');
const queue = 'payment_confirmation';

const url_amqp = 'amqp://user:password@localhost:5672';

const publishPaymentConfirmation = async (data) => {
  try {
    const connection = await amqp.connect(url_amqp);
    const channel = await connection.createChannel();
    
    await channel.assertQueue(queue, { durable: true });
    
    const message = JSON.stringify({ data });
    channel.sendToQueue(queue, Buffer.from(message), { persistent: true });
    
    console.log(`confirmation Payment. data sent: ${message}`);
    await channel.close();
    await connection.close();
  } catch (error) {
    console.error(`Failed to publish payment confirmation message: ${error}`);
  }
}

module.exports = publishPaymentConfirmation;