const asyncErrorHandler = require('../middlewares/asyncErrorHandler');
const Payment = require('../models/paymentModel');
const request = require('request');
const sendMessageToBroker = require('../utils/sendMessageToBroker');


// Process Payment
exports.processPayment = asyncErrorHandler(async (req, res, next) => {

    const { sum, orderId, userId, email, subject, text } = req.body;

    const payment = await Payment.create({
        sum,
        orderId,
        userId,
    });

    const options = {
        url: `${process.env.COMMANDE_SERVICE_URL}/`+orderId,
        method: 'PUT',
        json: true,
        body: req.body,
    };

    // Send request to User service
   request(options, (error, response, body) => {
    if (error) {
      console.error(error);
      res.status(500).send({ error: 'Internal Server Error' });
    } else if (response.statusCode !== 200) {
      console.log(response.body);
      res.status(response.statusCode).send(response.body);
    } else {

      //mail content
      const data = {
        to: email,
        subject: subject,
        text: text
      };

      sendMessageToBroker(data);

      // Send response
      res.status(201).json({
        success: true,
        payment,
        body
       });
      
    }
    
  });

});