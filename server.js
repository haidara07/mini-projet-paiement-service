const app = require('./app');
const connectDatabase = require('./config/database');

// UncaughtException Error
process.on('uncaughtException', (err) => {
    console.log(`Error: ${err.message}`);
    process.exit(1);
});

connectDatabase();

const PORT = process.env.PORT;
const server = app.listen(PORT, () => {
    console.log(`Payement Service --> Server running on ${process.env.PAYEMENT_SERVICE_URL}`)
});

// Unhandled Promise Rejection
process.on('unhandledRejection', (err) => {
    console.log(`Error: ${err.message}`);
    server.close(() => {
        process.exit(1);
    });
});
