const mongoose = require('mongoose');

const paymentSchema = new mongoose.Schema({
    sum: {
        type: Number,
        required: true,
    },
    orderId: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    userInfo: {
        address: {
            type: String,
        },
        mobile: {
            type: Number,
        },
        carte: {
            type: Number,
        },
        code: {
            type: Number,
        },
        zip: {
            type: Number,
        }
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Payment", paymentSchema);